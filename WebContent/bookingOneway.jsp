<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ page import="java.sql.*"%>
	<%
	String flyingFrom = request.getParameter("flyingFrom");
	String flyingTo = request.getParameter("flyingTo");
	String departing = request.getParameter("departing");
	String adults = request.getParameter("adults");
	String children = request.getParameter("children");
	String returning = "-";
	String type = "One-Way";
	Class.forName("com.mysql.jdbc.Driver");
	Connection con = DriverManager.getConnection("jdbc:mysql://localhost/Info", "InfoAdmin", "InfoAdmin");
	if (flyingFrom == "" || flyingTo == "" || departing == "" || returning == "" || adults == "" ||
			children == "") {
		response.sendRedirect("http://localhost:9420/Travelville/NotBooked.html");
	}
	if (flyingFrom != "" && flyingTo != "" && departing != "" && returning != "" && adults != "" &&
			children != "") {
		PreparedStatement stmt = con.prepareStatement("insert into ticket values(?,?,?,?,?,?,?)");
		stmt.setString(1, flyingFrom);
		stmt.setString(2, flyingTo);
		stmt.setString(3, departing);
		stmt.setString(4, returning);
		stmt.setString(5, adults);
		stmt.setString(6, children);
		stmt.setString(7, type);
		stmt.executeUpdate();
		response.sendRedirect("http://localhost:9420/Travelville/Payment.html");
	}
	%>
</body>
</html>