$(document).ready(function() {
    $('.button-collapse').sideNav();
    $('.slider').slider({
        indicators: false,
        height: 500,
        transition: 500,
        interval: 6000
    });
    $('input.autocomplete').autocomplete({
        data: {
            Aruba: null,
            'Cancun Mexico': null,
            Europe: null,
            Jamaica: null,
            Florida: null,
            'New York': null,
            Mumbai: null,
            Chennai: null,
            Kolkata: null,
            'New Delhi': null
        }
    });
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 15,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false,
        container: undefined,
        disable: [{ from: [2019, 1, 1], to: [2100, 1, 1] }]
    });
    $('.scrollspy').scrollSpy();
    $('.collapsible').collapsible();

    $(document).ready(function() {
        $('select').material_select();
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false, // Close upon selecting a date,
        container: undefined // ex. 'body' will append picker to body
    });
    $('.dropdown-button').dropdown({
        hover: true
    });
});