<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="js/jquery.js" type="text/javascript"></script>
</head>
<body>

<%@ page import="java.sql.*" %>
<%
	String name = request.getParameter("name1");
	String email = request.getParameter("email1");
	String phone = request.getParameter("phone1");
	String message = request.getParameter("message1");
	
	
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection con = DriverManager.getConnection("jdbc:mysql://localhost/Info", "InfoAdmin", "InfoAdmin");
	if (name == "" || email == "" || phone == "" || message == "") {
		response.sendRedirect("http://localhost:9420/Travelville/NotExecuted.html");
	}

	if (name != "" && email != "" && phone != "" && message != "" && message.length() < 120) {
		PreparedStatement stmt = con.prepareStatement("insert into feedback values(?,?,?,?)");
		stmt.setString(1, name);
		stmt.setString(2, email);
		stmt.setString(3, phone);
		stmt.setString(4, message);
		stmt.executeUpdate();
		response.sendRedirect("http://localhost:9420/Travelville/Executed.html");
	}
%>
</body>
</html>