<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ page import="java.sql.*"%>
	<%
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String phone = request.getParameter("phone");
		String gender = request.getParameter("gender");
		
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost/Info", "InfoAdmin", "InfoAdmin");
		if (firstName == "" || email == "" || phone == "" || lastName == "" || password == "" ||
				gender == "") {
			response.sendRedirect("http://localhost:9420/Travelville/NotRegistered.html");
		}
		if (firstName != "" && email != "" && phone != "" && lastName != "" && password != "" &&
				gender != "") {
			PreparedStatement stmt = con.prepareStatement("insert into registration values(?,?,?,?,?,?)");
			stmt.setString(1, firstName);
			stmt.setString(2, lastName);
			stmt.setString(3, email);
			stmt.setString(4, password);
			stmt.setString(5, phone);
			stmt.setString(6, gender);
			stmt.executeUpdate();
			response.sendRedirect("http://localhost:9420/Travelville/registered.html");
		}
	%>
</body>
</html>