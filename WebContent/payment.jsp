<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ page import="java.sql.*, java.util.*"%>
	<%
		Random rand = new Random();
		int rnum = 5000+rand.nextInt(2000);
		String cardtype = request.getParameter("card");
		String cardHolderName = request.getParameter("cardHolderName");
		String cardNumber = request.getParameter("cardNumber");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String cvv = request.getParameter("cvv");
		String price = String.valueOf(rnum);
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost/Info", "InfoAdmin", "InfoAdmin");
		if (cardtype == "" || cardHolderName == "" || cardNumber == "" || month == "" || year == "" ||
				cvv == "") {
			response.sendRedirect("http://localhost:9420/Travelville/NotPaid.html");
		}
		if (cardtype != "" && cardHolderName != "" && cardNumber != "" && month != "" && year != "" &&
				cvv != "") {
			PreparedStatement stmt = con.prepareStatement("insert into payment values(?,?,?,?,?,?,?)");
			stmt.setString(1, cardtype);
			stmt.setString(2, cardHolderName);
			stmt.setString(3, cardNumber);
			stmt.setString(4, month);
			stmt.setString(5, year);
			stmt.setString(6, cvv);
			stmt.setString(7, price);
			stmt.executeUpdate();
			response.sendRedirect("http://localhost:9420/Travelville/booking.html");
		}
	%>
</body>
</html>